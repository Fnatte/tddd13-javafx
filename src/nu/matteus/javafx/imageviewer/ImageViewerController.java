package nu.matteus.javafx.imageviewer;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.collections.ListChangeListener;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseDragEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.HBox;

public class ImageViewerController implements Initializable {

	@FXML
	private HBox thumbnails;
	
	@FXML
	private ImageView currentImage;
	
	private boolean isScaling;
	private double scalingStartX;
	private double scalingStartY;
	private double startScaleX;
	private double startScaleY;
	private double scalingFactor;
	
	@Override
	public void initialize(URL url, ResourceBundle resources) {
		
		currentImage.addEventHandler(ScrollEvent.ANY, currentImageScrollHandler);
		currentImage.addEventHandler(MouseEvent.ANY, currentImageMouseHandler);
		
		thumbnails.getChildren().addListener(thumbnailsChangeListener);
	}

	private EventHandler<MouseEvent> currentImageMouseHandler = new EventHandler<MouseEvent>() {
		
		@Override
		public void handle(MouseEvent event) {
			if(event.getEventType() == MouseEvent.DRAG_DETECTED) {
				isScaling = true;
				scalingStartX = event.getSceneX();
				scalingStartY = event.getSceneY();
				scalingFactor = Math.min(currentImage.getFitWidth(), currentImage.getImage().getWidth());
				startScaleX = currentImage.getScaleX();
				startScaleY = currentImage.getScaleX();
			} else if(event.getEventType() == MouseDragEvent.MOUSE_RELEASED) {
				isScaling = false;
			} else if(isScaling) {
				System.out.println("isScaling");
				double dx = scalingStartX - event.getSceneX();
				double dy = scalingStartY - event.getSceneY();
				currentImage.setScaleX(startScaleX + dx / scalingFactor);
				currentImage.setScaleY(startScaleY + dy / scalingFactor);
			}
		}
		
	};
	
	private EventHandler<ScrollEvent> currentImageScrollHandler = new EventHandler<ScrollEvent>() {
		
		@Override
		public void handle(ScrollEvent event) {
			currentImage.setRotate(
				currentImage.getRotate() + event.getDeltaY() * .25
			);
		}
	};
	
	private final EventHandler<MouseEvent> thumbnailClickHandler = new EventHandler<MouseEvent>() {
		
		@Override
		public void handle(MouseEvent event) {
			if(event.getSource() instanceof ImageView) {
				ImageView thumbnail = (ImageView)event.getSource();
				currentImage.setImage(thumbnail.getImage());
			}
			
		}
	};
	
	private final ListChangeListener<Node> thumbnailsChangeListener = new ListChangeListener<Node>() {

		@Override
		public void onChanged(
				javafx.collections.ListChangeListener.Change<? extends Node> c) {
			
			while(c.next()) {
				if(c.wasAdded()) {
					for(Node node : c.getAddedSubList()) {
						node.addEventHandler(MouseEvent.MOUSE_CLICKED, thumbnailClickHandler);
					}
				}
			}
		}
		
	};
}
