package nu.matteus.javafx.imageviewer;
import java.io.File;
import java.io.FilenameFilter;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

public class ImageViewer extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {
		
		// Get directory
		//DirectoryChooser directoryChooser = new DirectoryChooser();
		//File file = directoryChooser.showDialog(null);
		//if(file == null) return;
		File file = new File("E:/Users/Matteus/Pictures/mobil");
		
		// Get all images in the directory
		File[] files = file.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(".jpg") || name.endsWith(".png");
			}
		});
		
		// Load the first five files as images
		Image[] images = new Image[5];
		for(int i = 0; i < 5 && i < files.length; i++) {
			images[i] = new Image(files[i].toURI().toString());
		}
		
		// Load FXML
		Parent root = FXMLLoader.load(getClass().getResource("Window.fxml"));
		
		// Lookup nodes
		ImageView currentImage = (ImageView)root.lookup("#currentImage");
		HBox thumbnails = (HBox)root.lookup("#thumbnails");
		
		// Set the first image to the current
		currentImage.setImage(images[0]);
			
		// Create thumbnails
		for(int i = 0; i < 5; i++) {
			ImageView thumbnail = new ImageView(images[i]);
			thumbnail.setFitWidth(100);
			thumbnail.setPreserveRatio(true);
			thumbnails.getChildren().add(thumbnail);
		}
		
		primaryStage.setTitle("Image Viewer");
		primaryStage.setScene(new Scene(root));
		primaryStage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}
}
